from django.conf import settings
from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class AtlasOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    def create_user(self, claims):
        user = self.UserModel.objects.create_user(claims.get('email'), None, None)
        user.email = claims.get('email')
        user.external_id = claims.get('sub')
        user.save()

        user.is_active = False
        user.save()

        return user

    def update_user(self, user, claims):
        user.email = claims.get('email')
        user.external_id = claims.get('sub')
        user.save()

        return user

    def filter_users_by_claims(self, claims):
        users_by_external_id = self.UserModel.objects.filter(external_id__iexact=claims.get('sub'))
        if len(users_by_external_id) > 0:
            return users_by_external_id

        users_by_email = self.UserModel.objects.filter(external_id=None).filter(email__iexact=claims.get('email'))
        if len(users_by_email) > 0:
            return users_by_email

        return []
