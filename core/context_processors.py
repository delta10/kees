from django.conf import settings

def config(request):
    return {
        'AUTHENTICATION_ENABLE_CREDENTIALS': settings.AUTHENTICATION_ENABLE_CREDENTIALS,
        'AUTHENTICATION_ENABLE_OIDC': settings.AUTHENTICATION_ENABLE_OIDC
    }
